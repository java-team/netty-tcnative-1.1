Source: netty-tcnative-1.1
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends: automake,
               debhelper (>= 10),
               default-jdk,
               libapr1-dev,
               libmaven-antrun-plugin-java,
               libmaven-bundle-plugin-java,
               libssl1.0-dev,
               libtool,
               maven-debian-helper (>= 1.5)
Standards-Version: 3.9.8
Vcs-Git: https://anonscm.debian.org/git/pkg-java/netty-tcnative-1.1.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/netty-tcnative-1.1.git
Homepage: https://github.com/netty/netty-tcnative/

Package: libnetty-tcnative-1.1-java
Architecture: all
Depends: libnetty-tcnative-1.1-jni (>= ${source:Version}),
         ${maven:Depends},
         ${misc:Depends}
Suggests: ${maven:OptionalDepends}
Description: Tomcat native fork for Netty
 netty-tcnative is a fork of Tomcat Native, the native interface used by Tomcat
 to leverage the Apache Portable Runtime and provide superior scalability,
 performance, and better integration with native server technologies.
 .
 netty-tcnative includes a set of changes contributed by Twitter, Inc, such as:
  * Simplified distribution and linkage of native library
  * Complete mavenization of the project
  * Improved OpenSSL support

Package: libnetty-tcnative-1.1-jni
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Tomcat native fork for Netty (JNI library)
 netty-tcnative is a fork of Tomcat Native, the native interface used by Tomcat
 to leverage the Apache Portable Runtime and provide superior scalability,
 performance, and better integration with native server technologies.
 .
 netty-tcnative includes a set of changes contributed by Twitter, Inc, such as:
  * Simplified distribution and linkage of native library
  * Complete mavenization of the project
  * Improved OpenSSL support
 .
 This package contains the JNI library.
